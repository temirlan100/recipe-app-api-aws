# Commit #14
Наше приложение достаточно [готово и даже дружит с Travis CI](https://github.com/temirlan100/recipe-app-api). Но теперь мы будем делать более глубокий акцент на такой части как
DevOPS, в следующих коммитах мы постараемся все сделать по взрослому! =) Следующая связка технологий будет проходиться в коммитах:
* AWS technologies:
  * IAM
  * ECS
  * EC2
  * VPS
  * RDS
  * Application Load Balancer
  * S3
  * Certificate Manager
* Terraform
* GitLab
* Docker

Мы попытаемся создать в следующих коммитах [данную архитектуру](Architecture.png).
В данный момент для работы AWS необходимо привязать банковскую карту, так как приложение будет
потреблять реальные ресурсы для просчета вам поможет [калькулятор от AWS](https://calculator.aws/#/).

Хорошая практика когда есть такие окружения как: 
* Development
* Staging
* Production

Но и стоимость будет соответсвующая, но опять же все это индивидуально 
[один пример по расчетам](https://docs.google.com/spreadsheets/d/1VNup01_bFZRSKzra6FSo-WDDM1ZzL2IRmuq99NZ5WgY/edit#gid=0).

Так как вы уже сделали приложение у вас многое установлено, но еще потребуется поставить [aws-vault](https://github.com/99designs/aws-vault)
и можно настроить [ssh ключ](https://docs.gitlab.com/ee/user/ssh.html) для GitLab вы также можете делать действия git через https.

[Intro DevOps commit Click!](https://gitlab.com/temirlan100/recipe-app-api-aws/-/commit/83319abb9ce6833a79bd6f44c0c5f08d8059e2b5)

Материал с пользой к коммиту:

* [AWS Fargate Pricing](https://aws.amazon.com/fargate/pricing/)
* [AWS Fargate Pricing Calculator](http://fargate-pricing-calculator.site.s3-website-us-east-1.amazonaws.com/)

# Commit #15
Давайте перейдем к пользовательским настройкам нашего AWS аккаунта.
* Сделаем регистрацию
* Создадим IAM пользователя с правами администратора
* MFA policy для безопасности аккаунта.
* Установим AWS vault

Начать можно с того что [бесплатно](https://aws.amazon.com/free/?all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free%20Tier%20Types=*all&awsf.Free%20Tier%20Categories=*all) =) 
Далее проходим в нашу [консоль](https://us-east-1.console.aws.amazon.com/console/home?region=us-east-1&skipRegion=true#). 
Совет заходите через IAM, настройкой MFA и дайте пользователю доступ к финансовой информации, 
чтобы не подвергать угрозам своего root пользователя. Если честно следить и производить логин череp MFA у пользователя 
процедура затратная много кликов лучше это дело [автоматизировать](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_aws_my-sec-creds-self-manage.html) =)
Для этого:
1. Переходим в IAM сервис
2. Раздел Policy
3. Create policy
4. Выберите нужные настройки 
5. Посмотрите предварительные настройки в данном коммите и вставьте в Json вкладку.
6. Нажимаем Review policy
7. Заполняем name как имя файла
8. Нажимаем на create

Далее создадим группу чтобы было удобнее управлять пользователями:
1. Переходим в Groups
2. Create New Group
3. Даем имя 
4. Даем различные Policy (AdministratorAccess, ForceMFA)
5. Create Group

Теперь создадим IAM пользователя:
1. Выбираем вкладку _Users_
2. Вводим _User name_
3. Выбираем _Access type_
4. Устанавливаем _Console password_
5. Опционально ставим тэги(можно не ставить xD)
6. Нажимаем _Create user_

Теперь все готово можно произвести логин и тем самым сделать set MFA. Все через AWS console или WEB. 
Так вы увидите тот же интерфейс, что и при root пользователи. Переходим в _Users_ и во вкладку _security credentials_
и ставим MFA virtual очень удобен для меня, так как происходит скан QR через телефон.

[**Setup для AWS-vault.**](https://github.com/99designs/aws-vault#installing) Нам снова нужны _Users_ 
вкладка _security credentials_ и достать _Access key ID_, скачайте файл так как показываются секреты всего 1 раз.
В коммите будет снипет файл настроек по адресу _~/.aws/config_ поместите свои данные в данный файл.
Далее просто логин через aws-vault так же можете задать флаг `--duration=12h` чтобы быть авторизованным нужное время.

В настройках аккаунта есть вкладка _Billing & Cost Management Dashboard_ там вы увидите ваши расходы на hardware ресурсы, 
там возможно ставить различные лимиты, уведомления как финансовые, так и ресурсные.

[AWS user configuration Click!](https://gitlab.com/temirlan100/recipe-app-api-aws/-/commit/38a631087502d8c5fd652c66dca9891b9391489f)

# Commit #16
Начинаем варить Nginx =)
Nginx-приложение будет в [отдельном репозитории](https://gitlab.com/temirlan100/recipe-app-api-aws-proxy), 
прокся которая будет помогать управлять нашей нагрузкой. К примеру, клиент делает
запрос к сайту, просмотр главной страницы и ему нужно предоставить информацию текстовую или медиа, так вот нет необходимости
вообще грузить Django приложение, достаточно того чтобы прокси сервис перенаправил запрос в S3 для получения данных. 

Так что этот коммит будет максимально нацелен на [репозиторий прокси](https://gitlab.com/temirlan100/recipe-app-api-aws-proxy).
В репозитоии выставлены настройки на CI_CD что доступ к ним только у разработчиков данной репы, советую в будущем так же выставлять
так как репа открытая, пройдите по
* _Settings -> General -> Visibility, project features, permissions -> CI/CD  Build, test, and deploy your changes._
* _Settings -> CI_CD -> General pipelines -> Public pipelines_
* _Settings -> Repository ->_

Это базовые настройки, более детально вы можете посмотреть [в документации Gitlab](https://docs.gitlab.com/ee/ci/).

Наше прокси приложение будет в контейнере у которого будет свой образ для этого будет использовать службу 
[ECR в AWS](https://docs.aws.amazon.com/ecr/). Несколько моментов которые нужны будут в ECR:
* Выбрать регион дата центра (в верхнем левом углу интерфейса)
* Создать репозиторий (Get started)
* Выставить _Scan on push_
* Установить IAM пользователя для репозитория ECR

Займемся настройкой переменых сред в Gitlab _Settings -> CI_CD -> Variables_, необходимо установить:
* AWS_ACCESS_KEY_ID (protected, mask)
* AWS_SECRET_ACCESS_KEY (protected, mask)
* ECR_REPO (protected)

А вот теперь давайте делать конфиг Nginx! Произведем настройки, 
основные настройки и также [документацию по параметрам](https://uwsgi-docs.readthedocs.io/en/latest/Nginx.html#what-is-the-uwsgi-params-file).
Создадим _bash script_ и _Dockerfile_ [данный образ будет использоваться](https://hub.docker.com/r/nginxinc/nginx-unprivileged) 
для контейнера, попробуйте собрать контейнер локально через команду `docker build -t proxy .` 
Так же полезный калькулятор как [выставлять права](https://chmod-calculator.com/) к различным файлам в UNIX системах.

Предлагаю так же произвести настройки Pipeline для Gitlab CI_CD. Ниже будут полезные ссылки какой flow использовать 
и как настраивать:
* [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
* [Predefined variables reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
* [Yaml rules](https://docs.gitlab.com/ee/ci/yaml/#rules)

После успешного pipeline вы сможете увидеть ваш образ в ECR.

[Nginx & Gitlab CI_CD configurations click!](https://gitlab.com/temirlan100/recipe-app-api-aws-proxy/-/commit/0f2868c2d0ad1cee77e87349faacfcc95875e892)
